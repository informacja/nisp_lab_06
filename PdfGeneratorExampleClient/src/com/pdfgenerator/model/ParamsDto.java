package com.pdfgenerator.model;

import java.io.Serializable;

public class ParamsDto implements Serializable {
    public ParamsDto() {
    }

    public ParamsDto(Integer a, Integer b, Integer c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Integer getA() {
        return a;
    }

    public Integer getB() {
        return b;
    }

    public Integer getC() {
        return c;
    }

    private  Integer a;
    private  Integer b;
    private  Integer c;

}
